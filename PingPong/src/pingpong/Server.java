package pingpong;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server extends UnicastRemoteObject implements ServerIF {
	private static final long serialVersionUID = 1L;
	private Object callback = null;
	public Server() throws RemoteException {}
	public int register(Object callback) throws RemoteException {
		System.out.println("Client registered");
		this.callback=callback;
		return 0;
	}
	public int ping() throws RemoteException {
    	System.out.println("Ping received");
        ((ClientIF) callback).pong("Pong");
        System.out.println("Pong sent");
        return 0;
    }
    public static void main(String[] args) throws RemoteException {
        System.setProperty("java.rmi.server.hostname", args[0]);
        Server objServer = new Server();
        Registry reg = LocateRegistry.createRegistry(1111);
        reg.rebind("MyServer", objServer);
        System.out.println("Server Ready");
 
    }
}
