package pingpong;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Client extends UnicastRemoteObject implements ClientIF {
	private static final long serialVersionUID = 1L;
	public Client() throws RemoteException {}
		public static void main(String[] args)
				throws Exception {
	        System.setProperty("java.rmi.server.hostname", args[1]);
	        ServerIF server = (ServerIF) Naming.lookup("rmi://"+args[0]+":1111/MyServer");
	        server.register(new Client());
	        server.ping();
		}
		public void pong(String s) throws RemoteException {
			System.out.println(s+" received!");
			System.exit(0);
		}
	}
