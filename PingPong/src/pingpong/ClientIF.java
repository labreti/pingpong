package pingpong;

import java.rmi.Remote;
import java.rmi.RemoteException;
 
public interface ClientIF extends Remote {
    public void pong(String s) throws RemoteException;
}
