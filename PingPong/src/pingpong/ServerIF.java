package pingpong;

import java.rmi.Remote;
import java.rmi.RemoteException;
 
public interface ServerIF extends Remote {
	public int register(Object callback) throws RemoteException;
    public int ping() throws RemoteException;
}
